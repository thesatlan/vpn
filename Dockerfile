FROM ubuntu

RUN apt-get update
RUN apt-get install -y sudo
RUN apt-get install -y iproute2
RUN apt-get install -y iptables
RUN apt-get install -y net-tools
RUN apt-get install -y bridge-utils
RUN apt-get install -y iputils-ping
RUN apt-get install -y tcpdump

WORKDIR /root

COPY ./scripts/vpn.sh	./vpn.sh
COPY ./build/bin/vpn	./vpn

ENTRYPOINT ["./vpn.sh"]

