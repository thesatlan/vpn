#include <fcntl.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <net/route.h>
#include <net/if.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <linux/if.h>
#include <linux/if_tun.h>

#include "debug.h"
#include "interface.h"

#define TUN_FILE_PATH   ("/dev/net/tun")

int create_interface(int tun_tap, char *dev, const char *ip_addr, const char *ip_mask)
{
    struct ifreq ifr;
    int fd = open(TUN_FILE_PATH, O_RDWR);

    if(fd < 0)
    {
        return INTERFACE_ERROR;
    }

    memset(&ifr, 0, sizeof(ifr));

    /* Flags: IFF_TUN   - TUN device (no Ethernet headers) 
     *        IFF_TAP   - TAP device  
     *
     *        IFF_NO_PI - Do not provide packet information  
     */ 
    ifr.ifr_flags = (tun_tap == TUN) ? (IFF_TUN) : (IFF_TAP);
    ifr.ifr_flags |= IFF_NO_PI;

    if (dev != NULL)
    {
        strncpy(ifr.ifr_name, dev, IF_NAMESIZE);
    }

    // Create the device.
    if(ioctl(fd, TUNSETIFF, (void *)&ifr) < 0)
    {
        print_error("Create error\n%s", strerror(errno));
        close(fd);
        return INTERFACE_ERROR;
    }

    if (dev != NULL)
    {
        strncpy(dev, ifr.ifr_name, IF_NAMESIZE);
    }

    int iface_sock = socket(AF_INET, SOCK_DGRAM, 0);

    // Reading the current flags.
    if(ioctl(iface_sock, SIOCGIFFLAGS, (void *)&ifr) < 0)
    {
        print_error("Get error\n%s", strerror(errno));
        close(fd);
        return INTERFACE_ERROR;
    }

    // Adding the "up" flag.
    ifr.ifr_flags |= IFF_UP;
    
    if(ioctl(iface_sock, SIOCSIFFLAGS, (void *)&ifr) < 0)
    {
        print_error("Set error\n%s", strerror(errno));
        close(fd);
        return INTERFACE_ERROR;
    }

    struct sockaddr_in addr;
    
    if (ip_addr != NULL)
    {
        // Setting the IP.
        memset(&addr, 0, sizeof(addr));
        
        addr.sin_family = AF_INET;
        inet_pton(addr.sin_family, ip_addr, &addr.sin_addr);
        ifr.ifr_addr = *(struct sockaddr *)&addr;
        
        if (ioctl(iface_sock, SIOCSIFADDR, (void *)&ifr) < 0)
        {
            print_error("Set IP error\n%s", strerror(errno));
            close(fd);
            return INTERFACE_ERROR;
        }
    }

    if (ip_mask != NULL)
    {
        // Setting the mask.
        memset(&addr, 0, sizeof(addr));

        addr.sin_family = AF_INET;
        inet_pton(addr.sin_family, ip_mask, &addr.sin_addr);
        ifr.ifr_netmask = *(struct sockaddr *)&addr;

        if (ioctl(iface_sock, SIOCSIFNETMASK, (void *)&ifr) < 0)
        {
            print_error("Set mask error\n%s", strerror(errno));
            close(fd);
            return INTERFACE_ERROR;
        }
    }

    return fd;
}
