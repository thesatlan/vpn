#include <stdarg.h>
#include "debug.h"
#include "mbedtls/platform.h"

void print_message(FILE * stream, const char * format, va_list arglist);

void print_error(const char * format, ...)
{
    va_list arglist;
    va_start(arglist, format);

    print_message(stderr, format, arglist);

    va_end(arglist);
}

void print_info(const char * format, ...)
{
    va_list arglist;
    va_start(arglist, format);

    print_message(stdout, format, arglist);

    va_end(arglist);
}

void print_message(FILE * stream, const char * format, va_list arglist)
{
    vfprintf(stream, format, arglist);
    fprintf(stream, "\n");
    fflush(stream);
}

void debug_mbedtls(void *ctx, int level, const char *file, int line, const char *str)
{
    ((void) level);

    mbedtls_fprintf((FILE *) ctx, "%s:%04d: %s", file, line, str);
    fflush((FILE *)ctx);
}
