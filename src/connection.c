#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "connection.h"
#include "debug.h"
#include "mbedtls/error.h"
#include "mbedtls/sha1.h"

#define MIN(a,b) (((a) < (b)) ? (a) : (b))
#define HEX (16)

static size_t read_n(mbedtls_ssl_context* ssl, void *buffer, size_t n);

static size_t write_n(mbedtls_ssl_context* ssl, const void *buffer, size_t n);

void run_all_mbedtls_init_functions(MbedtlsParams * mbedtls)
{
    mbedtls_net_init(&mbedtls->listen_fd);
    mbedtls_net_init(&mbedtls->connection_fd);

    mbedtls_entropy_init(&mbedtls->entropy);
    mbedtls_ctr_drbg_init(&mbedtls->ctr_drbg);

    mbedtls_ssl_init(&mbedtls->ssl);
    mbedtls_ssl_config_init(&mbedtls->conf);
    mbedtls_x509_crt_init(&mbedtls->cert);
    mbedtls_pk_init(&mbedtls->pkey);

    print_info("Initialized mbedtls struct");
}

bool seed_drbg(MbedtlsParams * mbedtls)
{
    if (mbedtls_ctr_drbg_seed(&mbedtls->ctr_drbg, 
                              mbedtls_entropy_func, 
                              &mbedtls->entropy, 
                              NULL,
                              0) != 0)
    {
        print_error("Failed to initialize drbg seed");
        return false;
    }

    print_info("Seeded DRBG");

    return true;
}

bool parse_certificate_files(MbedtlsParams * mbedtls, 
                            int is_server, 
                            const unsigned char* certificate_path,
                            const unsigned char* private_key_file)
{
    if (mbedtls_x509_crt_parse_file(&mbedtls->cert, certificate_path) != 0)
    {
        print_error("Failed to parse the certificate file");
        return false;
    }

    if (is_server && mbedtls_pk_parse_keyfile(&mbedtls->pkey, private_key_file, NULL) != 0)
    {
        print_error("Failed to parse the private key file");
        return false;
    }

    print_info("Parsed certificate files");

    return true;
}

bool configure_defaults(MbedtlsParams * mbedtls, int is_server)
{
    if (mbedtls_ssl_config_defaults(&mbedtls->conf,
                                   is_server ? MBEDTLS_SSL_IS_SERVER : MBEDTLS_SSL_IS_CLIENT,
                                   MBEDTLS_SSL_TRANSPORT_STREAM,
                                   MBEDTLS_SSL_PRESET_DEFAULT) != 0)
    {
        print_error("Failed to configure ssl");
        return false;
    }

    if (!is_server)
    {
        mbedtls_ssl_conf_authmode(&mbedtls->conf, MBEDTLS_SSL_VERIFY_REQUIRED);
    }

    mbedtls_ssl_conf_rng(&mbedtls->conf, mbedtls_ctr_drbg_random, &mbedtls->ctr_drbg);
    mbedtls_ssl_conf_dbg(&mbedtls->conf, debug_mbedtls, stdout);

    print_info("Configured ssl defaults");

    return true;
}

bool setup_ssl(MbedtlsParams * mbedtls, int is_server, const unsigned char * server_name)
{
    mbedtls_ssl_conf_ca_chain(&mbedtls->conf, &mbedtls->cert, NULL);

    if (is_server && mbedtls_ssl_conf_own_cert(&mbedtls->conf, &mbedtls->cert, &mbedtls->pkey) != 0)
    {
        print_error("Failed to configure own certificate");
        return false;
    }

    if (mbedtls_ssl_setup(&mbedtls->ssl, &mbedtls->conf) != 0)
    {
        print_error("Failed to setup ssl with the configuration");
        return false;
    }

    if (!is_server && mbedtls_ssl_set_hostname(&mbedtls->ssl, server_name) != 0)
    {
        print_error("Failed to set server hostname");
        return false;
    }

    print_info("SSL was setup");

    return true;
}

bool init_connection_params(MbedtlsParams * mbedtls, 
                            int is_server, 
                            const unsigned char* certificate_path,
                            const unsigned char* private_key_file,
                            const unsigned char* server_name)
{
    run_all_mbedtls_init_functions(mbedtls);

    if (!seed_drbg(mbedtls) || 
        !parse_certificate_files(mbedtls, is_server, certificate_path, private_key_file) || 
        !configure_defaults(mbedtls, is_server) || 
        !setup_ssl(mbedtls, is_server, server_name))
    {
        return false;
    }

    print_info("Initialized connection params");

    return true;
}

void destroy_connection_params(MbedtlsParams * mbedtls)
{
    int ret = 0;
    while ((ret = mbedtls_ssl_close_notify(&mbedtls->ssl)) < 0)
    {
        if(ret != MBEDTLS_ERR_SSL_WANT_READ &&
           ret != MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            print_error("Failed to notify about SSL closure");
            // We don't return because it doesn't matter, and we need to free the parameters.
        }
    }

    mbedtls_pk_free(&mbedtls->pkey);
    mbedtls_x509_crt_free(&mbedtls->cert);
    mbedtls_ssl_config_free(&mbedtls->conf);
    mbedtls_ssl_free(&mbedtls->ssl);

    mbedtls_ctr_drbg_free(&mbedtls->ctr_drbg);
    mbedtls_entropy_free(&mbedtls->entropy);

    mbedtls_net_free(&mbedtls->connection_fd);
    mbedtls_net_free(&mbedtls->listen_fd);
}

bool execute_tls_handshake(MbedtlsParams * mbedtls, int is_server)
{
    int ret = 0;
    while ((ret = mbedtls_ssl_handshake(&mbedtls->ssl)) != 0)
    {
        if ( ret != MBEDTLS_ERR_SSL_WANT_READ && ret != MBEDTLS_ERR_SSL_WANT_WRITE )
        {
            print_error("TLS handshake failed (error code -0x%X)", -ret);

            return false;
        }
    }

    print_info("TLS handshake succeeded");

    if (!is_server)
    {
        uint32_t flags = 0;

        if ((flags = mbedtls_ssl_get_verify_result(&mbedtls->ssl)) != 0)
        {
            char verify_buffer[512];

            mbedtls_x509_crt_verify_info( verify_buffer, sizeof( verify_buffer ), "  ! ", flags );

            print_error("Failed to verify the server certificate");
            print_error("The reason is: %s", verify_buffer);
            return false;
        }

        print_info("Verified server certificate");
    }

    return true;
}

// Returns the connection socket.
bool connect_to_server(MbedtlsParams * mbedtls, 
                      char server_ip[], 
                      int server_port)
{
    char str_port[21] = {0};
    snprintf(str_port, sizeof(str_port), "%d", server_port);
    
    if(mbedtls_net_connect(&mbedtls->connection_fd, 
                           server_ip, 
                           str_port, 
                           MBEDTLS_NET_PROTO_TCP) != 0)
    {
        print_error("Failed to connect to server");
        return false;
    }

    mbedtls_ssl_set_bio(&mbedtls->ssl, &mbedtls->connection_fd, mbedtls_net_send, NULL, mbedtls_net_recv_timeout);

    return true;
}

bool wait_for_client(MbedtlsParams * mbedtls, 
                     int server_port)
{
    char str_port[21] = {0};
    snprintf(str_port, sizeof(str_port), "%d", server_port);

    if(mbedtls_net_bind(&mbedtls->listen_fd, 
                        NULL, 
                        str_port, 
                        MBEDTLS_NET_PROTO_TCP) != 0)
    {
        print_error("Failed to bind");
        return false;
    }

    if(mbedtls_net_accept(&mbedtls->listen_fd, 
                          &mbedtls->connection_fd, 
                          NULL, 
                          0, 
                          NULL) != 0 )
    {
        print_error("Failed to accept client");
        return false;
    }
    
    mbedtls_ssl_set_bio(&mbedtls->ssl, &mbedtls->connection_fd, mbedtls_net_send, NULL, mbedtls_net_recv_timeout);

    return true;
}

size_t read_n(mbedtls_ssl_context* ssl, void *buffer, size_t n)
{
    int _read = 0;
    size_t left = n;

    while (left > 0)
    {
        _read = mbedtls_ssl_read(ssl, buffer, left);

        if (_read == MBEDTLS_ERR_SSL_WANT_READ || _read == MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            continue;
        }
        else if (_read <= 0)
        {
            return 0;
        }

        left -= (size_t)_read;
        buffer += _read;
    }

    return n;
}

size_t write_n(mbedtls_ssl_context* ssl, const void *buffer, size_t n)
{
    int wrote = 0;
    size_t left = n;

    while (left > 0)
    {
        wrote = mbedtls_ssl_write(ssl, buffer, left);
        if (wrote == MBEDTLS_ERR_SSL_WANT_READ || wrote == MBEDTLS_ERR_SSL_WANT_WRITE)
        {
            continue;
        }
        else if (wrote <= 0)
        {
            return 0;
        }

        left -= (size_t)wrote;
        buffer += wrote;
    }

    return n;
}

uint16_t tunnel_read(mbedtls_ssl_context* ssl, char *buffer, uint16_t max_size)
{
    uint16_t message_length = 0;

    // Reading the message length.
    uint16_t _read = (uint16_t)read_n(ssl, &message_length, sizeof(message_length));
    if (_read != sizeof(message_length))
    {
        return 0;
    }

    // Converging the length from network order to processor order.
    message_length = ntohs(message_length);

    // In case the message is too long we read it max_size at a time.
    if (message_length > max_size)
    {
        uint16_t last_read = 0;
        _read = 0;

        while (_read < message_length)
        {
            last_read = MIN(message_length - _read, max_size);
            uint16_t cur_read = (uint16_t)read_n(ssl, buffer, last_read);
            if (cur_read != last_read)
            {
                // If the read didn't get all of the data an error occured.
                return 0;
            }

            _read += cur_read;
        }

        return last_read;
    }
    
    if (read_n(ssl, buffer, message_length) != message_length)
    {
        // If the read didn't get all of the data an error occured.
        return 0;
    }

    return message_length;
}

bool tunnel_write(mbedtls_ssl_context* ssl, const char * buffer, uint16_t message_length)
{
    // Converting to network order.
    message_length = htons(message_length);

    // Sending the message length and the message itself.
    if (write_n(ssl, &message_length, sizeof(message_length)) == 0 ||
        write_n(ssl, buffer, (size_t)ntohs(message_length)) == 0)
    {
        return false;
    }

    return true;
}
