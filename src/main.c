#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <net/if.h>

#include "debug.h"
#include "vpn.h"

#include "connection.h"

#define ENCRYPTED_MESSAGE ("Ok so this is the first message that is being sent through the open SSL connection.\nReally exciting development!")

int main(int argc, char **argv)
{
    int is_server = -1;
    char ip[HOST_NAME_MAX] = {0};
    int port = -1;
    int tun_tap = -1;
    char bridge[IF_NAMESIZE] = {0};

    // Getting the command line arguments.
    if (VPN_ARGS_SUCCESS != get_vpn_args(argc, 
                                         argv, 
                                         &is_server, 
                                         ip, 
                                         &port, 
                                         &tun_tap, 
                                         bridge))
    {
        return 1;
    }

    int interface_fd = prepare_machine(is_server, 
                                       tun_tap, 
                                       bridge);
    if (interface_fd == INTERFACE_ERROR)
    {
        return 1;
    }

    MbedtlsParams mbedtls;

    if (!init_connection_params(&mbedtls, is_server, "./certificates/server.crt", "./certificates/server.key", "localhost") ||
        !create_server_client_tunnel(&mbedtls, is_server, ip, port) || 
        !execute_tls_handshake(&mbedtls, is_server))
    {
        return 1;
    }

    if (!is_server)
    {
        print_info("Sending message...");
        tunnel_write(&mbedtls.ssl, ENCRYPTED_MESSAGE, sizeof(ENCRYPTED_MESSAGE));
    }
    else
    {
        char buffer[1000] = {0};
        if (tunnel_read(&mbedtls.ssl, buffer, sizeof(buffer)) != 0)
        {
            print_info("The received message is:\n%s", buffer);
        }
        else
        {
            print_error("Could not read message");
        }
        
    }
    
    print_info("Clear all and exit");
    destroy_connection_params(&mbedtls);

    // activate_vpn(tunnel_socket, interface_fd);

    // the socket and fd are closed inside "activate_vpn".
    return 0;
}
