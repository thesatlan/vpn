#include <stdlib.h>
#include <getopt.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <net/if.h>
#include <sys/select.h>

#include "debug.h"
#include "vpn.h"
#include "interface.h"
#include "connection.h"

#define MAX_COMMAND (1024)
#define BUFFER_SIZE (16384)

#define EMPTY_IS_SERVER (-1)

#define INTERFACE_CLIENT_IP ("11.0.0.3")
#define INTERFACE_SERVER_IP ("11.0.0.2")
#define INTERFACE_MASK ("255.255.255.0")

#define MAX(a,b) (((a) > (b)) ? (a) : (b))

static int is_server_flag;
static int tun_tap_flag;
static int vpn_on = true;

/**
 * @brief Checks if a given IP is valid or not.
 * 
 * Checking the given IP as a hostname as well as the X.X.X.X formation.
 * 
 * @param ipAddress [IN] The IP to check.
 * 
 * @return If the IP is valid or not.
 */
bool validate_ip_address(char *ipAddress);

/**
 * @brief Stopping the VPN when SIGINT is captured.
 * 
 * @param dummy [IN] A dummy argument.
 */
void stop_vpn(int dummy);

int get_vpn_args(
    int argc, 
    char ** argv, 
    int * is_server, 
    char * ip, 
    int * port,
    int * tun_tap,
    char * bridge)
{
    int option = 0;

    // Setting values to check if all needed parameters are given.
    is_server_flag = EMPTY_IS_SERVER;
    bool is_ip_set = false;
    bool is_port_set = false;
    tun_tap_flag = TAP;

    while (true)
    {
        static struct option long_options[] =
        {
          {"server",    no_argument, &is_server_flag, 1},
          {"client",    no_argument, &is_server_flag, 0},
          {"tun",       no_argument, &tun_tap_flag,   TUN},
          {"tap",       no_argument, &tun_tap_flag,   TAP},

          {"ip",        required_argument, 0, 'i'},
          {"port",      required_argument, 0, 'p'},
          {"bridge",    required_argument, 0, 'b'},
          {0, 0, 0, 0}
        };
        
        int option_index = 0;
        option = getopt_long(argc, argv, "i:p:b:", long_options, &option_index);

        // Detect the end of the options.
        if (option == -1)
        {
            break;
        }

        switch (option)
        {
        case 0:
            // Flag already set.
            break;

        case 'i':
            strncpy(ip, optarg, HOST_NAME_MAX);

            if (!validate_ip_address(ip))
            {
                print_error("Invalid IP");
                return VPN_ARGS_ERROR;
            }

            is_ip_set = true;
            break;

        case 'p':
            *port = atoi(optarg);
            
            if (port == 0)
            {
                print_error("Invalid port");
                return VPN_ARGS_ERROR;
            }

            is_port_set = true;
            break;

        case 'b':
            strncpy(bridge, optarg, IF_NAMESIZE);
            break;

        case '?': // FALLTHROUGH
        default:
            return VPN_ARGS_ERROR;
        }
    }

    // Moving the values to their arguments.
    *is_server = is_server_flag;
    *tun_tap = tun_tap_flag;

    if (*is_server == EMPTY_IS_SERVER)
    {
        print_error("Error: You must specify --server or --client");
        return VPN_ARGS_ERROR;
    }
    if (*is_server && !is_port_set)
    {
        print_error("Error: Servers must get port to listen to");
        return VPN_ARGS_ERROR;
    }
    if (!*is_server && (!is_ip_set || !is_port_set))
    {
        print_error("Error: Clients must get IP and port to connect to");
        return VPN_ARGS_ERROR;
    }

    return VPN_ARGS_SUCCESS;
}

bool validate_ip_address(char *ipAddress)
{
    struct sockaddr_in sa;
    
    if (inet_pton(AF_INET, ipAddress, &(sa.sin_addr)))
    {
        return true;
    }

    struct hostent *he;
    he = gethostbyname(ipAddress);
    if (he != NULL)
    {
        strcpy(ipAddress, inet_ntoa(*(struct in_addr *)he->h_addr_list[0]));
        return true;
    }

    return false;
}

int prepare_machine(int is_server, 
                    int tun_tap, 
                    char * bridge)
{
    char command[MAX_COMMAND] = {0};

    char interface_device[IF_NAMESIZE] = {0};
    int interface_fd = 0;

    if (is_server)
    {
        if (tun_tap == TUN)
        {
            // Creating the tun interface.
            interface_fd = create_interface(
                                TUN,
                                interface_device, 
                                INTERFACE_SERVER_IP, 
                                INTERFACE_MASK);
            if (interface_fd == INTERFACE_ERROR)
            {
                print_error("Interface error");
                return INTERFACE_ERROR;
            }

            // Setting forwarding rules so that packets could go through a NAT table.
            system("sysctl -w net.ipv4.ip_forward=1 > /dev/null");
            system("iptables -P FORWARD ACCEPT");
            system("iptables -t nat -A POSTROUTING -j MASQUERADE");
        }
        else if (tun_tap == TAP)
        {
            // Creating the tap interface.
            interface_fd = create_interface(
                                TAP,
                                interface_device,
                                NULL,
                                NULL);
            if (interface_fd == INTERFACE_ERROR)
            {
                print_error("Interface error");
                return INTERFACE_ERROR;
            }

            if (strnlen(bridge, IF_NAMESIZE) != 0)
            {
                // Connecting to the given bridge.
                sprintf(command, "brctl addif %s %s", bridge, interface_device);
                system(command);
            }
        }
    }
    else
    {
        if (tun_tap == TUN)
        {
            // Creating the tun/tap interface.
            interface_fd = create_interface(
                                tun_tap,
                                interface_device, 
                                INTERFACE_CLIENT_IP, 
                                INTERFACE_MASK);
            if (interface_fd == INTERFACE_ERROR)
            {
                print_error("Interface error");
                return INTERFACE_ERROR;
            }
        }
        else
        {
            // Creating the tun/tap interface.
            interface_fd = create_interface(
                                tun_tap,
                                interface_device,
                                NULL,
                                NULL);
            if (interface_fd == INTERFACE_ERROR)
            {
                print_error("Interface error");
                return INTERFACE_ERROR;
            }
        }
    }

    return interface_fd;
}

bool create_server_client_tunnel(MbedtlsParams * mbedtls, int is_server, char * ip, int port)
{
    // Creating the connection socket, both for server and client.
    bool tunnel_connected = is_server ? 
                    wait_for_client(mbedtls, port):
                    connect_to_server(mbedtls, ip, port);
    
    if (!tunnel_connected)
    {
        print_error("Socket error");
        return false;
    }
    print_info("Tunnel connected");

    return true;
}
/*
void activate_vpn(int tunnel_socket,
                  int interface_fd)
{
    char buffer[BUFFER_SIZE];
    fd_set fds;
    uint16_t nread = 0;
    int ret = 0;
    int max_fd = MAX(tunnel_socket, interface_fd);

    signal(SIGINT, stop_vpn);

    // This code works for the server and the client because
    // they both just moving packets from the interface to 
    // the tunnel and vice versa.
    while (vpn_on)
    {
        FD_ZERO(&fds);
        FD_SET(tunnel_socket, &fds);
        FD_SET(interface_fd, &fds);

        // Waiting until the tunnel or interface have something in read.
        ret = select(max_fd + 1, &fds, NULL, NULL, NULL);
        if (ret < 0 && errno == EINTR)
        {
            continue;
        }

        if (FD_ISSET(tunnel_socket, &fds))
        {
            // Got input from the tunnel, forward it to the interface.
            nread = tunnel_read(tunnel_socket, buffer, sizeof(buffer));
            if (nread <= 0)
            {
                print_error("Tunnel closed");
                break;
            }
            write(interface_fd, buffer, (size_t)nread);
        }
        if (FD_ISSET(interface_fd, &fds))
        {
            // Got input from the interface, forward it to the tunnel.
            nread = (uint16_t)read(interface_fd, buffer, sizeof(buffer));
            if (nread <= 0)
            {
                // This means the interface has been closed. weird...
                print_error("Interface closed?!");
                break;
            }
            tunnel_write(tunnel_socket, buffer, nread);
        }
    }

    close(tunnel_socket);
    close(interface_fd);
}*/

void stop_vpn(int dummy)
{
    print_info(""); // Printing one black line to skip the "^C" text.
    print_info("Got SIGINT, Closing the Vpn...");
    vpn_on = false;
}
