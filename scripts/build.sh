mkdir -p build
cd build

    mkdir -p native
    cd native
    
        echo "==== Building native target ===="
        cmake -DCMAKE_C_COMPILER=gcc ../..
        cmake --build .
        
    cd ..

    mkdir -p mips
    cd mips
    
        echo "==== Building mips target ===="
        cmake -DCMAKE_C_COMPILER=mips-linux-gnu-gcc ../..
        cmake --build .
        
    cd ..

cd ..
