#!/bin/bash

TARGET=""
INTERFACE=""
IP_OPT=""
IP=""
PORT_OPT=""
PORT=""
BRIDGE_OPT=""
BRIDGE=""
PID=""

usage() {
  echo "Usage: $0 [-S (Server), -C (Client), -I (Isolated)]
        [-N (TUN), -P (TAP)] [-i ip] [-p port] [-b bridge]"
}

exit_abnormal() {
  usage
  exit 1
}

# Checking inputs.
while getopts ":SCINPi:p:b:" options; do
  case "${options}" in
    S)
      TARGET="--server"
      ;;
    C)
      TARGET="--client"
      ;;
    I)
      TARGET=""
      ;;
    N)
      INTERFACE="--tun"
      ;;
    P)
      INTERFACE="--tap"
      ;;
    i)
      IP_OPT="-i"
      IP="${OPTARG}"
      ;;
    p)
      PORT_OPT="-p"
      PORT="${OPTARG}"
      ;;
    b)
      BRIDGE_OPT="-b"
      BRIDGE="${OPTARG}"
      ;;
    :)
      echo "Error: -${OPTARG} requires an argument."
      exit_abnormal
      ;;
    *)
      exit_abnormal
      ;;
  esac
done

# Printing telemetry.
echo INTERFACES:
ip -4 -o a
echo ROUTES:
ip route

# Running the VPN.
if [ "$TARGET" == "--server" ]; then
  if [ "$INTERFACE" == "--tap" ]; then
    brctl addbr $BRIDGE
    brctl addif $BRIDGE eth1
    ifconfig eth1 0.0.0.0 promisc up
    ifconfig $BRIDGE server
  fi
  
  sudo ./vpn $TARGET $INTERFACE $IP_OPT $IP $PORT_OPT $PORT $BRIDGE_OPT $BRIDGE &

  sleep 10

elif [ "$TARGET" == "--client" ]; then
  sleep 1

  echo "Starting direct pings..."
  ping -O -W 1 -c 4 isolated

  echo "Connecting to VPN..."
  sudo ./vpn $TARGET $INTERFACE $IP_OPT $IP $PORT_OPT $PORT $BRIDGE_OPT $BRIDGE &
  sleep 1

  echo "Starting pings through the VPN..."
  ping -O -W 1 -c 8 -I tun0 isolated

elif [ "$TARGET" == "" ]; then
  sleep 15
fi

echo "Exiting..."

