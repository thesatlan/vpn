./scripts/build.sh || { echo "Build failed!" ; exit 1; }
./scripts/generate_certificates.sh || { echo "Certification creation failed!" ; exit 1; }
./scripts/docker.sh || { echo "Docker image creation failed!" ; exit 1; }
docker-compose up

