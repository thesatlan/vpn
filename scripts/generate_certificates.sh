
openssl req \
  -newkey rsa:4096 \
  -x509 \
  -sha256 \
  -days 365 \
  -nodes \
  -out certificates/server.crt \
  -keyout certificates/server.key \
  -subj "/C=IL/ST=IL/L=Rishon Le-Zion/O=MyVPN/OU=Encryption/CN=localhost"
  
