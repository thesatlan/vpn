#pragma once

#include <stdbool.h>

#include "mbedtls/net.h"
#include "mbedtls/entropy.h"
#include "mbedtls/ctr_drbg.h"

#define CONNECTION_FAILURE (-1)

typedef struct MbedtlsParams_s
{
    mbedtls_net_context listen_fd;
    mbedtls_net_context connection_fd;

    mbedtls_entropy_context entropy;
    mbedtls_ctr_drbg_context ctr_drbg;
    mbedtls_ssl_context ssl;
    mbedtls_ssl_config conf;
    mbedtls_x509_crt cert;
    mbedtls_pk_context pkey;
} MbedtlsParams;

bool init_connection_params(MbedtlsParams * mbedtls, 
                            int is_server, 
                            const unsigned char* certificate_path,
                            const unsigned char* private_key_file,
                            const unsigned char* server_name);

void destroy_connection_params(MbedtlsParams * mbedtls);

bool execute_tls_handshake(MbedtlsParams * mbedtls, int is_server);

bool connect_to_server(MbedtlsParams * mbedtls, 
                       char server_ip[], 
                       int server_port);


bool wait_for_client(MbedtlsParams * mbedtls, 
                     int server_port);

uint16_t tunnel_read(mbedtls_ssl_context* ssl, char *buffer, uint16_t max_size);

bool tunnel_write(mbedtls_ssl_context* ssl, const char * buffer, uint16_t message_length);
