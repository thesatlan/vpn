#pragma once

#include <stdio.h>

/**
 * @brief Prints an error message to stderr.
 * 
 * @param format [IN] The format of the print
 * @param ... [IN] The format arguments
 */
void print_error(const char * format, ...);

/**
 * @brief Prints an info message to stdout.
 * 
 * @param format [IN] The format of the print
 * @param ... [IN] The format arguments
 */
void print_info(const char * format, ...);

void debug_mbedtls(void *ctx, int level, const char *file, int line, const char *str);
