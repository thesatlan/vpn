#pragma once

#include "vpn.h"

#define INTERFACE_ERROR (-1)

/**
 * @brief Create a TUN interface
 * 
 * This function creates a TUN interface, activates 
 * it, and giving it the desired IP and mask.
 * 
 * @param tun_tap [IN] Should the created interface be TUN or TAP.
 * @param dev [OUT] Buffer of size IF_NAMESIZE to be filled with the device name.
 * @param ip_addr [IN] The desired ip address on the interface.
 * @param ip_mask [IN] The desired ip mask on the interface.
 * 
 * @return The file descriptor of the interface.
 */
int create_interface(int tun_tap, 
                     char *dev, 
                     const char *ip_addr, 
                     const char *ip_mask);
