
#pragma once

#include <stdbool.h>
#include <arpa/inet.h>
#define __USE_POSIX 1 // For HOST_NAME_MAX
#include <limits.h>

#include "connection.h"

#define VPN_ARGS_SUCCESS    (0)
#define VPN_ARGS_ERROR      (1)

#define TAP                 (0)
#define TUN                 (1)

#define SOCKET_ERROR        (-1)
#define INTERFACE_ERROR     (-1)

/**
 * @brief Gets the arguments for the VPN program.
 * 
 * @param argc [IN] The arguments count.
 * @param argv [IN] The arguments array.
 * @param is_server [OUT] Returns if this is a server run (1) or a client run (0).
 * @param ip [OUT] Returns the ip of the server to connect to. (Needed for clients only)
 * @param port [OUT] Returns the port to open for servers or the port to connect to for clients.
 * @param tun_tap [OUT] Returns if the VPN should be TUN or TAP based.
 * @param bridge [OUT] Returns the bridge name to connect TAP to (optional).
 * 
 * @return VPN_ARGS_SUCCESS on success.
 * @return VPN_ARGS_ERROR when an error occures.
 */
int get_vpn_args(
    int argc, 
    char ** argv, 
    int * is_server, 
    char * ip, 
    int * port,
    int  *tun_tap,
    char * bridge);

/**
 * @brief Opening the tun/tap interface and running commands to prepare the machine
 * 
 * Preparing the machine also includes setting up the iptables in the server
 * so that the pachets will be forwarded freely.
 * 
 * @param is_server [IN] Is this a server run (1) or a client run (0).
 * @param tun_tap [IN] Is the VPN TUN or TAP based.
 * @param bridge [IN] The bridge name to connect TAP to (optional).
 * 
 * @return The file descriptor of the created interface.
 */
int prepare_machine(int is_server, 
                    int tun_tap, 
                    char * bridge);


bool create_server_client_tunnel(MbedtlsParams * mbedtls, int is_server, char * ip, int port);

/**
 * @brief Activating the message forwarding between the tunnel and the interface.
 * 
 * This function is a loop that connects the tunnel and the interface together.
 * Each packet that reaches one of them is forwarded to the other as is.
 * 
 * @param tunnel_socket [IN] The socket that connects the client and the server.
 * @param interface_fd [IN] The file descriptor of the created interface.
 * 
 * @note tunnel_socket and interface_fd will be closed when this function terminates.
 */
void activate_vpn(int tunnel_socket,
                  int interface_fd);
